<?php

namespace Pantagruel74\Uuid;

interface UuidToolInterface
{
    /**
     * @return string
     */
    public function getUuid(): string;

    /**
     * @param string $uuid
     * @return bool
     */
    public function isValid(string $uuid): bool;
}