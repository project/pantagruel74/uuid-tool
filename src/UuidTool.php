<?php

namespace Pantagruel74\Uuid;

use Ramsey\Uuid\Uuid;

class UuidTool implements UuidToolInterface
{
    /**
     * @return string
     */
    public function getUuid(): string
    {
        return Uuid::uuid4()->toString();
    }

    /**
     * @param string $uuid
     * @return bool
     */
    public function isValid(string $uuid): bool
    {
        return Uuid::isValid($uuid);
    }
}