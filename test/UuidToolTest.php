<?php

class UuidToolTest extends \PHPUnit\Framework\TestCase
{
    public function testUuid()
    {
        $uuidTool = new \Pantagruel74\Uuid\UuidTool();
        $uuids = [];
        for($i = 0; $i < 10000; $i++)
        {
            $uuid = $uuidTool->getUuid();
            $this->assertNotEmpty($uuid);
            $this->assertTrue($uuidTool->isValid($uuid));
            $this->assertFalse(in_array($uuid, $uuids));
            $uuids[] = $uuid;
        }
        $this->assertEquals(count($uuids), 10000);
    }
}